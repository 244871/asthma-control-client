package com.astma;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.astma.Views.Logowanie;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class UstawieniaLekiTest {

    @Rule
    public ActivityTestRule<Logowanie> mActivityTestRule = new ActivityTestRule<>(Logowanie.class);

    @Test
    public void ustawieniaLekiTest() {
        ViewInteraction editText = onView(
                allOf(withId(R.id.podany_login),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                4),
                        isDisplayed()));
        editText.perform(click());

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.podany_login),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                4),
                        isDisplayed()));
        editText2.perform(replaceText("P"), closeSoftKeyboard());

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.podane_haslo),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                6),
                        isDisplayed()));
        editText3.perform(replaceText("p"), closeSoftKeyboard());

        ViewInteraction button = onView(
                allOf(withId(R.id.zaloguj), withText("Zaloguj się"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                9),
                        isDisplayed()));
        button.perform(click());

        ViewInteraction button2 = onView(
                allOf(withId(R.id.ustawienia), withText("USTAWIENIA"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                12),
                        isDisplayed()));
        button2.perform(click());

        ViewInteraction button3 = onView(
                allOf(withId(R.id.ustawienia_leki_os), withText("Zmień zażywane leki rozszerzające oskrzela"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                7),
                        isDisplayed()));
        button3.perform(click());

        ViewInteraction imageView = onView(
                allOf(withId(R.id.imageView),
                        withParent(withParent(withId(android.R.id.content))),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction button4 = onView(
                allOf(withId(R.id.potwierdz_os), withText("POTWIERDŹ"),
                        withParent(allOf(withId(R.id.constraint_layout3),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        button4.check(matches(isDisplayed()));

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.podana_nowa_nazwa_os),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout3),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                3),
                        isDisplayed()));
        editText4.perform(replaceText("Symbicort"), closeSoftKeyboard());

        ViewInteraction editText5 = onView(
                allOf(withId(R.id.podana_nowa_nazwa_os), withText("Symbicort"),
                        withParent(allOf(withId(R.id.constraint_layout3),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        editText5.check(matches(withText("Symbicort")));

        ViewInteraction button5 = onView(
                allOf(withId(R.id.potwierdz_os), withText("potwierdź"),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout3),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                4),
                        isDisplayed()));
        button5.perform(click());

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.imageView),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        imageView2.perform(click());

        ViewInteraction button6 = onView(
                allOf(withId(R.id.ustawienia_leki_pz), withText("Zmień zażywane leki przeciwzapalne"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                8),
                        isDisplayed()));
        button6.perform(click());

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.imageView),
                        withParent(withParent(withId(android.R.id.content))),
                        isDisplayed()));
        imageView3.check(matches(isDisplayed()));

        ViewInteraction button7 = onView(
                allOf(withId(R.id.potwierdz_pz), withText("POTWIERDŹ"),
                        withParent(allOf(withId(R.id.constraint_layout4),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        button7.check(matches(isDisplayed()));

        ViewInteraction editText6 = onView(
                allOf(withId(R.id.podana_nowa_nazwa_pz),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout4),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                3),
                        isDisplayed()));
        editText6.perform(replaceText("Aspulmo"), closeSoftKeyboard());

        ViewInteraction editText7 = onView(
                allOf(withId(R.id.podane_nowe_dawki_rano),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout4),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                5),
                        isDisplayed()));
        editText7.perform(replaceText("1"), closeSoftKeyboard());

        ViewInteraction editText8 = onView(
                allOf(withId(R.id.podane_nowe_dawki_noc),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout4),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                9),
                        isDisplayed()));
        editText8.perform(replaceText("2"), closeSoftKeyboard());

        ViewInteraction editText9 = onView(
                allOf(withId(R.id.podana_nowa_nazwa_pz), withText("Aspulmo"),
                        withParent(allOf(withId(R.id.constraint_layout4),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        editText9.check(matches(withText("Aspulmo")));

        ViewInteraction editText10 = onView(
                allOf(withId(R.id.podane_nowe_dawki_rano), withText("1"),
                        withParent(allOf(withId(R.id.constraint_layout4),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        editText10.check(matches(withText("1")));

        ViewInteraction editText11 = onView(
                allOf(withId(R.id.podane_nowe_dawki_noc), withText("2"),
                        withParent(allOf(withId(R.id.constraint_layout4),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        editText11.check(matches(withText("2")));

        ViewInteraction button8 = onView(
                allOf(withId(R.id.podane_nowe_godziny_rano), withText("Podaj godzinę rano"),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout4),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                7),
                        isDisplayed()));
        button8.perform(click());

        ViewInteraction radioButton = onView(
                allOf(withClassName(is("android.widget.RadioButton")), withText("AM"),
                        childAtPosition(
                                allOf(withClassName(is("android.widget.RadioGroup")),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                3)),
                                0),
                        isDisplayed()));
        radioButton.perform(click());

        ViewInteraction imageButton2 = onView(
                allOf(withClassName(is("android.widget.ImageButton")), withContentDescription("Switch to text input mode for the time input."),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        4),
                                0),
                        isDisplayed()));
        imageButton2.perform(click());

        ViewInteraction spinner = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.Spinner.class),
                        withParent(allOf(IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class)))),
                        isDisplayed()));
        spinner.check(matches(isDisplayed()));

        ViewInteraction imageButton3 = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.ImageButton.class), withContentDescription("Switch to clock mode for the time input."),
                        withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
                        isDisplayed()));
        imageButton3.check(matches(isDisplayed()));

        ViewInteraction button10 = onView(
                allOf(withId(R.id.ok), withText("Wprowadź"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.custom),
                                        0),
                                2),
                        isDisplayed()));
        button10.perform(click());

        ViewInteraction button11 = onView(
                allOf(withId(R.id.podane_nowe_godziny_noc), withText("Podaj godzinę wieczorem"),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout4),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                11),
                        isDisplayed()));
        button11.perform(click());

        ViewInteraction button12 = onView(
                allOf(withId(R.id.ok), withText("Wprowadź"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.custom),
                                        0),
                                2),
                        isDisplayed()));
        button12.perform(click());

        ViewInteraction button13 = onView(
                allOf(withId(R.id.potwierdz_pz), withText("potwierdź"),
                        childAtPosition(
                                allOf(withId(R.id.constraint_layout4),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                6)),
                                12),
                        isDisplayed()));
        button13.perform(click());

        ViewInteraction imageView4 = onView(
                allOf(withId(R.id.imageView),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        imageView4.perform(click());

        ViewInteraction imageView5 = onView(
                allOf(withId(R.id.imageView),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        imageView5.perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
