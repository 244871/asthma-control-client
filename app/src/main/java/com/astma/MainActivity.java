package com.astma;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.StrictMode;

import com.astma.Controllers.AutorisationController;
import com.astma.Controllers.PacjentPzController;
import com.astma.DTOs.GodzinyDTO;
import com.astma.DTOs.PacjentDTO;
import com.astma.Notification.MyReceiver;
import com.astma.SharedPreference.SaveSharedPreference;
import com.astma.Views.Glowny;
import com.astma.Views.Glowny_Lekarz;
import com.astma.Views.Logowanie;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MainActivity extends AppCompatActivity {

    AutorisationController autorisationController;

    ClipboardManager myClipboard;
    String login;
    PacjentDTO pacjent_DTO;
    PacjentPzController pacjentPzController;

    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Intent alarmIntent;

    DateTimeFormatter formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if(SaveSharedPreference.getUserName(MainActivity.this).length() == 0) { //jeśli żaden użytkownik nie jest już zalogowany

            Intent i = new Intent(this, Logowanie.class);
            startActivity(i);
        }
        else {
            formatter = DateTimeFormatter.ISO_LOCAL_TIME;
            login = SaveSharedPreference.getUserName(this);

            pacjentPzController = new PacjentPzController();
            autorisationController = new AutorisationController();
            if(autorisationController.amIaDoctor(login)){
                Intent i = new Intent(this, Glowny_Lekarz.class);
                startActivity(i);
            }
            else{
                Intent i = new Intent(this, Glowny.class);
                startActivity(i);

            }
        }
    }
    public Calendar setCalendar(LocalTime godzina){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, godzina.getHour());
        calendar.set(Calendar.MINUTE, godzina.getMinute());
        calendar.set(Calendar.SECOND, 0);
        if(godzina.getHour()<12) {
            calendar.set(Calendar.AM_PM, Calendar.AM);
        } else{
            calendar.set(Calendar.AM_PM,Calendar.PM);
        }

        return calendar;
    }
}