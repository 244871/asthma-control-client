package com.astma.Notification;

import android.icu.util.Calendar;

import java.time.LocalTime;

public class SetCalendarOnHour {
    public Calendar setCalendar(LocalTime godzina){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, godzina.getHour());
        calendar.set(Calendar.MINUTE, godzina.getMinute());
        calendar.set(Calendar.SECOND, 0);
        if(godzina.getHour()<12) {
            calendar.set(Calendar.AM_PM, Calendar.AM);
        } else{
            calendar.set(Calendar.AM_PM,Calendar.PM);
        }

        return calendar;
    }
}
