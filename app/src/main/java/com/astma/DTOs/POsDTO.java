package com.astma.DTOs;

public class POsDTO {
    private String nazwa;
    private String login;

    public POsDTO(String nazwa, String login) {
        this.nazwa = nazwa;
        this.login = login;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
