package com.astma.DTOs;

public class NewPacjentDTO {
    private LogPassDTO logPassDTO;
    private String lekarz_login;

    public NewPacjentDTO(LogPassDTO logPassDTO, String lekarz_login) {
        this.logPassDTO = logPassDTO;
        this.lekarz_login = lekarz_login;
    }

    public LogPassDTO getLogPassDTO() {
        return logPassDTO;
    }

    public void setLogPassDTO(LogPassDTO logPassDTO) {
        this.logPassDTO = logPassDTO;
    }

    public String getLekarz_login() {
        return lekarz_login;
    }

    public void setLekarz_login(String lekarz_login) {
        this.lekarz_login = lekarz_login;
    }
}
