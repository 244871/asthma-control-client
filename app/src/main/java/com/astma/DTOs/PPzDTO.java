package com.astma.DTOs;

public class PPzDTO extends Lek_pzDTO{
    private String login;

    public PPzDTO(String nazwa, int dawki_rano, int dawki_noc, String godz_rano, String godz_noc, String login) {
        super(nazwa, dawki_rano, dawki_noc, godz_rano, godz_noc);
        this.login = login;
    }

    public PPzDTO(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
