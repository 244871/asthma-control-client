package com.astma.DTOs;

public class ZazucieDTO extends NewZazucieDTO{
    private String lek_pz_nazwa;
    private String lek_os_nazwa;

    public ZazucieDTO(String date, int dawka, String pacjent_login, String lek_pz_nazwa, String lek_os_nazwa) {
        super(date, dawka, pacjent_login);
        this.lek_pz_nazwa = lek_pz_nazwa;
        this.lek_os_nazwa = lek_os_nazwa;
    }

    public ZazucieDTO(String date, int dawka, String pacjent_login) {
        super(date, dawka, pacjent_login);
    }

    public String getLek_pz_nazwa() {
        return lek_pz_nazwa;
    }

    public void setLek_pz_nazwa(String lek_pz_nazwa) {
        this.lek_pz_nazwa = lek_pz_nazwa;
    }

    public String getLek_os_nazwa() {
        return lek_os_nazwa;
    }

    public void setLek_os_nazwa(String lek_os_nazwa) {
        this.lek_os_nazwa = lek_os_nazwa;
    }
}
