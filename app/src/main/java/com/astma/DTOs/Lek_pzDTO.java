package com.astma.DTOs;

public class Lek_pzDTO {
    private String nazwa;
    private int dawki_rano;
    private int dawki_noc;
    private String godz_rano;
    private String godz_noc;

    public Lek_pzDTO(String nazwa, int dawki_rano, int dawki_noc, String godz_rano, String godz_noc) {
        this.nazwa = nazwa;
        this.dawki_rano = dawki_rano;
        this.dawki_noc = dawki_noc;
        this.godz_rano = godz_rano;
        this.godz_noc = godz_noc;
    }

    public Lek_pzDTO() {
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getDawki_rano() {
        return dawki_rano;
    }

    public void setDawki_rano(int dawki_rano) {
        this.dawki_rano = dawki_rano;
    }

    public int getDawki_noc() {
        return dawki_noc;
    }

    public void setDawki_noc(int dawki_noc) {
        this.dawki_noc = dawki_noc;
    }

    public String getGodz_rano() {
        return godz_rano;
    }

    public void setGodz_rano(String godz_rano) {
        this.godz_rano = godz_rano;
    }

    public String getGodz_noc() {
        return godz_noc;
    }

    public void setGodz_noc(String godz_noc) {
        this.godz_noc = godz_noc;
    }
}
