package com.astma.DTOs;

public class GodzinyDTO {
    String godzRano;
    String godzNoc;

    public GodzinyDTO(String godzRano, String godzNoc) {
        this.godzRano = godzRano;
        this.godzNoc = godzNoc;
    }

    public String getGodzRano() {
        return godzRano;
    }

    public void setGodzRano(String godzRano) {
        this.godzRano = godzRano;
    }

    public String getGodzNoc() {
        return godzNoc;
    }

    public void setGodzNoc(String godzNoc) {
        this.godzNoc = godzNoc;
    }
}
