package com.astma.DTOs;

public class Lek_osDTO {

    private String nazwa;

    public Lek_osDTO() {
    }

    public Lek_osDTO(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
