package com.astma.DTOs;

public class PacjentDTO {
    private NewPacjentDTO newPacjentDTO;
    private String lek_os_nazwa;
    private String lek_pz_DTO_nazwa;

    public PacjentDTO(NewPacjentDTO newPacjentDTO, String lek_os_nazwa, String lek_pz_DTO_nazwa) {
        this.newPacjentDTO = newPacjentDTO;
        this.lek_os_nazwa = lek_os_nazwa;
        this.lek_pz_DTO_nazwa = lek_pz_DTO_nazwa;
    }

    public NewPacjentDTO getNewPacjentDTO() {
        return newPacjentDTO;
    }

    public void setNewPacjentDTO(NewPacjentDTO newPacjentDTO) {
        this.newPacjentDTO = newPacjentDTO;
    }

    public String getLek_os_nazwa() {
        return lek_os_nazwa;
    }

    public void setLek_os_nazwa(String lek_os_nazwa) {
        this.lek_os_nazwa = lek_os_nazwa;
    }

    public String getLek_pz_DTO_nazwa() {
        return lek_pz_DTO_nazwa;
    }

    public void setLek_pz_DTO_nazwa(String lek_pz_DTO_nazwa) {
        this.lek_pz_DTO_nazwa = lek_pz_DTO_nazwa;
    }
}
