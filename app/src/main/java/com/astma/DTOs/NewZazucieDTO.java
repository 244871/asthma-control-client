package com.astma.DTOs;

public class NewZazucieDTO {
    private String date;
    private int dawka;
    private String pacjent_login;

    public NewZazucieDTO(String date, int dawka, String pacjent_login) {
        this.date = date;
        this.dawka = dawka;
        this.pacjent_login = pacjent_login;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDawka() {
        return dawka;
    }

    public void setDawka(int dawka) {
        this.dawka = dawka;
    }

    public String getPacjent_login() {
        return pacjent_login;
    }

    public void setPacjent_login(String pacjent_login) {
        this.pacjent_login = pacjent_login;
    }
}
