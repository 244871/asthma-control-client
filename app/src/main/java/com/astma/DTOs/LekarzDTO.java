package com.astma.DTOs;

public class LekarzDTO {

    private String login;
    private String password;

    public LekarzDTO(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public LekarzDTO() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
