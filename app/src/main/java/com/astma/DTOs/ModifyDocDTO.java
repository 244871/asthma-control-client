package com.astma.DTOs;

public class ModifyDocDTO {String login;
    String login_lekarz;

    public ModifyDocDTO(String login, String login_lekarz) {
        this.login = login;
        this.login_lekarz = login_lekarz;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin_lekarz() {
        return login_lekarz;
    }

    public void setLogin_lekarz(String login_lekarz) {
        this.login_lekarz = login_lekarz;
    }
}
