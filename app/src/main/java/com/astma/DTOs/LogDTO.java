package com.astma.DTOs;

public class LogDTO {
    private String login;

    public LogDTO(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
