package com.astma.Controllers;

import com.astma.DTOs.GodzinyDTO;
import com.astma.DTOs.PPzDTO;
import com.astma.DTOs.ZazucieDTO;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;

public class PacjentPzController {

    IPAddress ipAddress = new IPAddress();

    private String localhost = ipAddress.getLocalhost()+"/pacjentpz";

    HttpHeaders headers = new HttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    String url;
    String general = "/pacjentpz";

    public void modify( PPzDTO pPzDTO){
        url = ipAddress.createUri(general, "/modify");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PPzDTO> httpEntity= new HttpEntity<>(pPzDTO);
        PPzDTO pPzDTOs = restTemplate.postForObject(url, httpEntity, PPzDTO.class);
    }

    //dostajemy godziny zażywania leków przez danego pacjenta
    public GodzinyDTO getGodziny(String login){
        UriComponentsBuilder builder = ipAddress.buildUri1(general, "/getGodziny", "login", login);
        return restTemplate.getForObject(builder.toUriString(), GodzinyDTO.class);
    }
}
