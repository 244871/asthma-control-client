package com.astma.Controllers;

import com.astma.DTOs.POsDTO;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class PacjentOsController {

    IPAddress ipAddress = new IPAddress();

    HttpHeaders headers = new HttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    String url;
    String general = "/pacjentos";

    public void modify(POsDTO pOsDTO){
        url =ipAddress.createUri(general, "/modify");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<POsDTO> httpEntity= new HttpEntity<>(pOsDTO);
        POsDTO pOsDTOs = restTemplate.postForObject(url, httpEntity, POsDTO.class);
    }
}
