package com.astma.Controllers;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class AutorisationController {
    IPAddress ipAddress = new IPAddress();

    RestTemplate restTemplate = new RestTemplate();
    String general = "/authorisation";

    public Boolean loginExistence(String login){
        UriComponentsBuilder builder = ipAddress.buildUri1(general, "/login/exist", "login", login);
        return restTemplate.getForObject(builder.toUriString(), Boolean.TYPE);
    }

    public Boolean verify( String login, String password){
        UriComponentsBuilder builder = ipAddress.buildUri1(general,"/verify", "login", login);
                builder.queryParam("password", password);
        return restTemplate.getForObject(builder.toUriString(), Boolean.TYPE);
    }

    public Boolean amIaDoctor( String login){
        UriComponentsBuilder builder = ipAddress.buildUri1(general,"/doctor", "login", login);
        return restTemplate.getForObject(builder.toUriString(), Boolean.TYPE);
    }
}
