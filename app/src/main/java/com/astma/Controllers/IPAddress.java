package com.astma.Controllers;

import org.springframework.web.util.UriComponentsBuilder;

public class IPAddress {
    private String localhost = "http://192.168.1.46:8080";

    public String getLocalhost() {
        return localhost;
    }

    public UriComponentsBuilder buildUri1(String general, String api, String passingName, String passingValue){
        return UriComponentsBuilder.fromHttpUrl(createUri(general, api))
                .queryParam(passingName, passingValue);
    }

    public String createUri(String general, String api){
        return localhost+general+api;
    }
}
