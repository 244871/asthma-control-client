package com.astma.Controllers;

import com.astma.DTOs.LogDTO;
import com.astma.DTOs.LogPassDTO;
import com.astma.DTOs.ModifyDocDTO;
import com.astma.DTOs.NewPacjentDTO;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collections;

public class UsersController {
    IPAddress ipAddress = new IPAddress();

    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    String url;
    String general = "/users";

//dodajemy nowego pacjenta
    public void add(NewPacjentDTO newPacjent_DTO){
        url = ipAddress.createUri(general, "/add");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<NewPacjentDTO> httpEntity= new HttpEntity<>(newPacjent_DTO);
        NewPacjentDTO pOsDTOs = restTemplate.postForObject(url, httpEntity, NewPacjentDTO.class);
    }

//dodajemy nowego lekarza. W tym programie niepotrzebne, lekarzy tworrzy tylko właściciel servera
    public void addL(LogPassDTO newLekarzDTO){
        url = ipAddress.createUri(general, "/addL");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LogPassDTO> httpEntity= new HttpEntity<>(newLekarzDTO);
        LogPassDTO newLekarzDTOs = restTemplate.postForObject(url, httpEntity, LogPassDTO.class);
    }

//dostajemy wszystkich lekarzy jacy istnieją
    public ArrayList<LogDTO> getAllDoctors(){
        url = ipAddress.createUri(general, "/getDoctors");
        LogDTO[] doctors = restTemplate.getForObject(url, LogDTO[].class);
        ArrayList<LogDTO> logDTOArrayList = logDTOArrayList(doctors);
        return logDTOArrayList;
    }

    //dostajemy pajentów przypisanych do danego lekarza
    public ArrayList<LogDTO> getPacjents(String lekarz_login){
        UriComponentsBuilder builder = ipAddress.buildUri1(general, "/getPacjentsForDoctor", "lekarz_login", lekarz_login);
        LogDTO[] pacjents = restTemplate.getForObject(builder.toUriString(), LogDTO[].class);
        ArrayList<LogDTO> logDTOArrayList = logDTOArrayList(pacjents);
        return logDTOArrayList;
    }

    //zmieniamy hasło pacjenta
    public void modifyPassword(LogPassDTO pacjent_DTO){
        url = ipAddress.createUri(general, "/modify/password");
        headers.setContentType(MediaType.APPLICATION_JSON);
        restTemplate.put(url, pacjent_DTO);
    }

    //zmianiamy lekarza przypisanego pacjentowi
    public void modifyDoctor(ModifyDocDTO modifyDocDTO){
        url = ipAddress.createUri(general, "/modify/doctor");
        headers.setContentType(MediaType.APPLICATION_JSON);
        restTemplate.put(url, modifyDocDTO);
    }

    public ArrayList<LogDTO> logDTOArrayList(LogDTO[] logDTOS){
        ArrayList<LogDTO> logDTOArrayList = new ArrayList<>();
        Collections.addAll(logDTOArrayList, logDTOS);
        return logDTOArrayList;
    }


}
