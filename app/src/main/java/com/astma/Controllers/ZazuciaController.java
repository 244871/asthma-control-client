package com.astma.Controllers;

import com.astma.DTOs.NewZazucieDTO;
import com.astma.DTOs.ZazucieDTO;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collections;

public class ZazuciaController {
    IPAddress ipAddress = new IPAddress();

    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    String url;
    String general = "/zazucia";

    //dodajemy nowe zażycie leku
    public void add(NewZazucieDTO newZazucieDTO){
        url = ipAddress.createUri(general, "/add");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<NewZazucieDTO> httpEntity= new HttpEntity<>(newZazucieDTO);
        NewZazucieDTO newZazucieDTO1 = restTemplate.postForObject(url, httpEntity, NewZazucieDTO.class);
    }

    //dostajemy wszystkie zażycia pacjenta z danego przedziału czasu
    public ArrayList<ZazucieDTO> allForPacjent(String login, int dni){
        UriComponentsBuilder builder = ipAddress.buildUri1(general, "/forDays", "login", login);
                builder.queryParam("dni", dni);
        ArrayList<ZazucieDTO> zazucieDTOArrayList = toArray(builder);
        return zazucieDTOArrayList;
    }

    //dostajemy zażycia pacjenta z dzisiaj
    public ZazucieDTO allForPacjentToday(String login){
        UriComponentsBuilder builder = ipAddress.buildUri1(general, "/today", "login", login);
        ArrayList<ZazucieDTO> zazucieDTOArrayList = toArray(builder);
        return zazucieDTOArrayList.get(0);
    }

    public ArrayList<ZazucieDTO> toArray(UriComponentsBuilder builder){
        ZazucieDTO[] zazucieDTOS = restTemplate.getForObject(builder.toUriString(), ZazucieDTO[].class);
        ArrayList<ZazucieDTO> zazucieDTOArrayList = new ArrayList<>();
        Collections.addAll(zazucieDTOArrayList, zazucieDTOS);
        return zazucieDTOArrayList;
    }
}
