package com.astma.Views;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.astma.Controllers.PacjentPzController;
import com.astma.DTOs.GodzinyDTO;
import com.astma.DTOs.PPzDTO;
import com.astma.Notification.MyReceiver;
import com.astma.Notification.SetCalendarOnHour;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Ustawienia_leki_pz extends Activity {

    String login;
    EditText nazwa;
    EditText dawkaR;
    EditText dawkaW;
    Button godzR;
    Button godzW;

    Button Submit;
    AlertDialog.Builder builder;
    LayoutInflater layoutinflater;
    TimePicker timePicker;
    AlertDialog alertdialog;
    String godz_rano;
    String godz_noc;

    PacjentPzController pacjentPzController;
    int czyRejestracja;

    String hour;
    String min;

    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Intent alarmIntent;
    SetCalendarOnHour setCalendar;
    DateTimeFormatter formatter;
    GodzinyDTO godzinyDTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ustawienia_leki_pz);
        Intent intent = getIntent();
        czyRejestracja = intent.getIntExtra("czyRejestracja", 0);
        login = SaveSharedPreference.getUserName(this);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        pacjentPzController = new PacjentPzController();

        if(czyRejestracja==1){
            ImageView button = findViewById(R.id.imageView);
            button.setVisibility(View.INVISIBLE);
        }

        nazwa = findViewById(R. id.podana_nowa_nazwa_pz);
        dawkaR = findViewById(R.id.podane_nowe_dawki_rano);
        dawkaW = findViewById(R.id.podane_nowe_dawki_noc);
        godzR = findViewById(R.id.podane_nowe_godziny_rano);
        godzW = findViewById(R.id.podane_nowe_godziny_noc);

    }

    public void godzRano(View view) {
        getGodzina(1);
    }

    public void godzWieczor(View view) {
        getGodzina(0);
    }


    public void potwierdz_pz(View view) {
        if (TextUtils.isEmpty(nazwa.getText())||TextUtils.isEmpty(dawkaR.getText())||TextUtils.isEmpty(dawkaW.getText())||TextUtils.isEmpty(godzW.getText())){
            Toast.makeText(getApplicationContext(),"Wprowadź dane",Toast.LENGTH_LONG).show();

        }else {
            pacjentPzController.modify(new PPzDTO(nazwa.getText().toString(), Integer.parseInt(dawkaR.getText().toString()),
                    Integer.parseInt(dawkaW.getText().toString()), godz_rano, godz_noc, login));
            Toast.makeText(getApplicationContext(), "Zmiany zostały pomyślnie wprowadzone", Toast.LENGTH_SHORT).show();

            //a teraz stworzymy powiadomienie
            godzinyDTO = pacjentPzController.getGodziny(login);
            formatter = DateTimeFormatter.ISO_LOCAL_TIME;
            alarmIntent = new Intent(this, MyReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            setCalendar = new SetCalendarOnHour();
            Calendar calendar = setCalendar.setCalendar(LocalTime.parse(godzinyDTO.getGodzRano(), formatter));
            Calendar calendar2 = setCalendar.setCalendar(LocalTime.parse(godzinyDTO.getGodzNoc(), formatter));
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar2.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

            if(czyRejestracja==1){

                Intent i = new Intent(this, Glowny.class);
                startActivity(i);
            }
        }
    }

    public void powrot(View view) {

        Intent i = new Intent(this, Ustawienia.class);
        startActivity(i);
    }

    private void getGodzina(int czyRano){
        builder = new AlertDialog.Builder(this);
        layoutinflater = getLayoutInflater();
        View Dview = layoutinflater.inflate(R.layout.pop_up_godzina, null);
        builder.setCancelable(true);
        builder.setView(Dview);
        timePicker = Dview.findViewById(R.id.tp);
        Submit = (Button) Dview.findViewById(R.id.ok);

        alertdialog = builder.create();

        Submit.setOnClickListener(v -> {

            alertdialog.cancel();
            if((String.valueOf(timePicker.getHour()).length()==1)){
                hour=("0"+(timePicker.getHour()));
            }else{
                hour=(String.valueOf(timePicker.getHour()));
            }
            if((String.valueOf(timePicker.getMinute()).length()==1)){
                min=("0"+(timePicker.getMinute()));
            }else{
                min=(String.valueOf(timePicker.getMinute()));
            }

            if(czyRano==1){
                godz_rano=(hour+":"+min+":00");
            }else{
                godz_noc=(hour+":"+min+":00");
            }

        });
        alertdialog.show();
    }

}


