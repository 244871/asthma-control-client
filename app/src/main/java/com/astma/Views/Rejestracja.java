package com.astma.Views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.astma.Controllers.AutorisationController;
import com.astma.Controllers.UsersController;
import com.astma.General.SpinnerGetter;
import com.astma.MainActivity;
import com.astma.DTOs.LogPassDTO;
import com.astma.DTOs.NewPacjentDTO;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;

public class Rejestracja extends Activity {
    EditText loginET;
    EditText hasloET;
    Spinner wyborLekarz;
    String [] items;
    String selected;

    AutorisationController autorisationController;
    UsersController usersController;
    SpinnerGetter spinnerGetter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rejestracja);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        loginET = findViewById(R.id.podany_login);
        hasloET = findViewById(R.id.podane_haslo);
        wyborLekarz = findViewById(R.id.podany_lekarz);

        usersController = new UsersController();
        autorisationController = new AutorisationController();
        spinnerGetter = new SpinnerGetter();

        items = spinnerGetter.getLekarz();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        wyborLekarz.setAdapter(adapter);
        wyborLekarz.setSelection(0);
    }

    public void back(View view) {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void register(View view) {
        if (TextUtils.isEmpty(loginET.getText())||TextUtils.isEmpty(hasloET.getText())||wyborLekarz.getSelectedItemId()==0){
            Toast.makeText(getApplicationContext(),"Wprowadź dane",Toast.LENGTH_LONG).show();

        }else {
            selected = wyborLekarz.getSelectedItem().toString();
            if (autorisationController.loginExistence(loginET.getText().toString())) { //zwraca false jeśli użytkownik nie istnieje
                Toast.makeText(getApplicationContext(), "Podany login już istnieje. Wybierz inny login", Toast.LENGTH_SHORT).show();
            } else {
                usersController.add(new NewPacjentDTO(new LogPassDTO(loginET.getText().toString(), hasloET.getText().toString()), selected));
                SaveSharedPreference.setUserName(this, loginET.getText().toString());
                Intent i = new Intent(this, Ustawienia_leki_os.class);
                i.putExtra("czyRejestracja", 1);
                startActivity(i);
            }
        }
    }
}

