package com.astma.Views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.astma.Controllers.PacjentOsController;
import com.astma.DTOs.POsDTO;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;

public class Ustawienia_leki_os extends Activity {

    String login;
    EditText nazwa;
    PacjentOsController pacjentOsController;

    int czyRejestracja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ustawienia_leki_os);
        Intent intent = getIntent();
        czyRejestracja = intent.getIntExtra("czyRejestracja", 0);
        login = SaveSharedPreference.getUserName(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        pacjentOsController = new PacjentOsController();

        if(czyRejestracja==1){
            ImageView imageView = findViewById(R.id.imageView);
            imageView.setVisibility(View.INVISIBLE);
        }

        nazwa = findViewById(R. id.podana_nowa_nazwa_os);

    }

    public void potwierdz_os(View view) {
        if (TextUtils.isEmpty(nazwa.getText())){
            Toast.makeText(getApplicationContext(),"Wprowadź dane",Toast.LENGTH_LONG).show();
        }else {
            pacjentOsController.modify(new POsDTO(nazwa.getText().toString(), login));
            Toast.makeText(getApplicationContext(), "Zmiany zostały pomyślnie wprowadzone", Toast.LENGTH_SHORT).show();
            if(czyRejestracja==1){
                Intent i = new Intent(this, Ustawienia_leki_pz.class);
                i.putExtra("czyRejestracja", 1);
                startActivity(i);
            }
        }
    }

    public void powrot(View view) {
        Intent i = new Intent(this, Ustawienia.class);
        startActivity(i);
    }


}
