package com.astma.Views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.astma.Controllers.PacjentPzController;
import com.astma.DTOs.GodzinyDTO;
import com.astma.General.ChartSettings;
import com.astma.Controllers.UsersController;
import com.astma.Controllers.ZazuciaController;
import com.astma.DTOs.ZazucieDTO;
import com.astma.General.Przyciski;
import com.astma.Notification.MyReceiver;
import com.astma.Notification.SetCalendarOnHour;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarEntry;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Glowny extends Activity {

    int wprowadzona_ilosc_dawek;
    Button Submit;
    AlertDialog.Builder builder;
    LayoutInflater layoutinflater;
    EditText edittext;
    AlertDialog alertdialog;
    private BarChart barChart;
    ArrayList<ZazucieDTO> zazucieS;
    ZazucieDTO zazucieSDzis;
    TextView textView;
    ArrayList<BarEntry> entries;
    String login;
    ZazuciaController zazuciaController;
    UsersController usersController;
    DateTimeFormatter dateTimeFormatter;
    ChartSettings chartSettings;
    Przyciski przyciski;
    Button tydzienb;
    Button miesiacb;
    Button rokb;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.glowny);

        dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        login = SaveSharedPreference.getUserName(this);
        tydzienb = findViewById(R.id.tydzien);
        miesiacb = findViewById(R.id.miesiac);
        rokb = findViewById(R.id.rok);
        textView = findViewById(R.id.wyniki_teraz);

        zazuciaController = new ZazuciaController();
        usersController = new UsersController();
        chartSettings = new ChartSettings();
        przyciski = new Przyciski();
        zazucieSDzis = zazuciaController.allForPacjentToday(login);


        textView.setText("Dzisiaj użyłeś inhalator\n"+ zazucieSDzis.getDawka()+" razy");

        barChart = (BarChart)findViewById(R.id.chart);
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  7, login);
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);

    }

    public void tydzien(View view) {
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  7, login);
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }

    public void miesiac(View view) {
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,30, login);
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }

    public void rok(View view) {
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,365, login);
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }

    public void dadaj_uzycie(View view) {
        builder = new AlertDialog.Builder(this);
        layoutinflater = getLayoutInflater();
        View Dview = layoutinflater.inflate(R.layout.pop_up_zazucie,null);
        builder.setCancelable(true);
        builder.setView(Dview);

        edittext = (EditText) Dview.findViewById(R.id.editText1);
        Submit = (Button) Dview.findViewById(R.id.wprowadz);

        alertdialog = builder.create();

        Submit.setOnClickListener(v -> {

            alertdialog.cancel();
            if (TextUtils.isEmpty(edittext.getText())){
                Toast.makeText(getApplicationContext(),"Wprowadź wartość",Toast.LENGTH_SHORT).show();
                alertdialog.show();

            }else {
                wprowadzona_ilosc_dawek = Integer.parseInt(edittext.getText().toString());
                zazuciaController.add(new ZazucieDTO(LocalDate.now().format(dateTimeFormatter), wprowadzona_ilosc_dawek, login));
                Toast.makeText(getApplicationContext(), "Zażycie zostało dodane", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }
        });

        alertdialog.show();
    }




    public void ustawienia(View view) {
        Intent i = new Intent(this, Ustawienia.class);
        i.putExtra("czyRejestracja", 0);
        startActivity(i);
    }
    public void wyloguj(View view) {
        SaveSharedPreference.clearUserName(this);
        Intent i = new Intent(this, Logowanie.class);
        startActivity(i);
    }

}

