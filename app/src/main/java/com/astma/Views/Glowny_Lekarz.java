package com.astma.Views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import com.astma.General.ChartSettings;
import com.astma.Controllers.UsersController;
import com.astma.Controllers.ZazuciaController;
import com.astma.DTOs.ZazucieDTO;
import com.astma.General.Przyciski;
import com.astma.General.SpinnerGetter;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarEntry;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class Glowny_Lekarz extends Activity {

    private BarChart barChart;
    ArrayList<ZazucieDTO> zazucieS;
    ZazucieDTO zazucieSDzis;
    TextView textView;
    ArrayList<BarEntry> entries;

    Spinner wyborPacjent;
    String [] items;

    String login;

    ZazuciaController zazuciaController;
    UsersController usersController;
    DateTimeFormatter dateTimeFormatter;

    ChartSettings chartSettings;
    Przyciski przyciski;
    SpinnerGetter spinnerGetter;

    Button tydzienb;
    Button miesiacb;
    Button rokb;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.glowny_l);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        login = SaveSharedPreference.getUserName(this);
        tydzienb = findViewById(R.id.tydzien);
        miesiacb = findViewById(R.id.miesiac);
        rokb = findViewById(R.id.rok);

        dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        zazuciaController = new ZazuciaController();
        usersController = new UsersController();
        chartSettings = new ChartSettings();
        przyciski = new Przyciski();
        spinnerGetter = new SpinnerGetter();

        wyborPacjent = findViewById(R.id.wybrany_pacjent);

        items = spinnerGetter.getPacjents(login);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        wyborPacjent.setAdapter(adapter);

        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  7, wyborPacjent.getSelectedItem().toString());
        zazucieSDzis = zazuciaController.allForPacjentToday(wyborPacjent.getSelectedItem().toString());

        textView = findViewById(R.id.wyniki_teraz);
        textView.setText("Dzisiaj użyto inhalator\n"+ zazucieSDzis.getDawka()+" razy");

        onItem();
        barChart = findViewById(R.id.chart);
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }


    public void onItem (){
        wyborPacjent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  7, wyborPacjent.getSelectedItem().toString());
                zazucieSDzis = zazuciaController.allForPacjentToday(wyborPacjent.getSelectedItem().toString());
                chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }

    public void tydzien(View view) {
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  7, wyborPacjent.getSelectedItem().toString());
        zazucieSDzis = zazuciaController.allForPacjentToday(wyborPacjent.getSelectedItem().toString());
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }

    public void miesiac(View view) {
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  30, wyborPacjent.getSelectedItem().toString());
        zazucieSDzis = zazuciaController.allForPacjentToday(wyborPacjent.getSelectedItem().toString());
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }

    public void rok(View view) {
        zazucieS = przyciski.przyciski(tydzienb, miesiacb, rokb,  365, wyborPacjent.getSelectedItem().toString());
        zazucieSDzis = zazuciaController.allForPacjentToday(wyborPacjent.getSelectedItem().toString());
        chartSettings.draw(barChart, zazucieS,  entries, zazucieSDzis, textView);
    }

    public void wyloguj(View view) {
        SaveSharedPreference.clearUserName(this);
        Intent i = new Intent(this, Logowanie.class);
        startActivity(i);
    }

}
