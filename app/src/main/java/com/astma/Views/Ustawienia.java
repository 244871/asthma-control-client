package com.astma.Views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.astma.Controllers.UsersController;
import com.astma.DTOs.LogPassDTO;
import com.astma.DTOs.ModifyDocDTO;
import com.astma.General.SpinnerGetter;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;

public class Ustawienia extends Activity {

    EditText hasloET;
    EditText hasloET2;
    Spinner wyborLekarz;
    String [] items;
    String login;

    UsersController usersController;
    SpinnerGetter spinnerGetter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ustawienia);
        login = SaveSharedPreference.getUserName(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        usersController = new UsersController();
        spinnerGetter = new SpinnerGetter();

        hasloET = findViewById(R.id.podane_haslo1);
        hasloET2 = findViewById(R.id.podane_haslo2);
        wyborLekarz = findViewById(R.id.podany_lekarz_n);

        items = spinnerGetter.getLekarz();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        wyborLekarz.setAdapter(adapter);
        wyborLekarz.setSelected(false);
    }

    public void potwierdz(View view) {
        if(!TextUtils.isEmpty(hasloET.getText())||!TextUtils.isEmpty(hasloET2.getText())){//znaczy się chcemy zmienić hasło
            if (hasloET.getText().toString().equals(hasloET2.getText().toString())) {
                usersController.modifyPassword(new LogPassDTO(login, hasloET.getText().toString()));
                Toast.makeText(getApplicationContext(), "Zmiany zostały pomyślnie wprowadzone", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Podane hasła są różne", Toast.LENGTH_SHORT).show();
            }

        }else if(wyborLekarz.getSelectedItemId()!=0){
            usersController.modifyDoctor(new ModifyDocDTO(login, wyborLekarz.getSelectedItem().toString()));
            Toast.makeText(getApplicationContext(), "Zmiany zostały pomyślnie wprowadzone", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(getApplicationContext(),"Wprowadź dane",Toast.LENGTH_LONG).show();
        }
    }


    public void powrot(View view) {
        Intent i = new Intent(this, Glowny.class);
        startActivity(i);
    }

    public void ustawienia_leki_os(View view) {
        Intent i = new Intent(this, Ustawienia_leki_os.class);
        startActivity(i);
    }

    public void ustawienia_leki_pz(View view) {
        Intent i = new Intent(this, Ustawienia_leki_pz.class);
        startActivity(i);
    }
}
