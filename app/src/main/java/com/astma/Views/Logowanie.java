package com.astma.Views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.astma.Controllers.AutorisationController;
import com.astma.MainActivity;
import com.astma.R;
import com.astma.SharedPreference.SaveSharedPreference;

public class Logowanie extends Activity {

    EditText loginET;
    EditText hasloET;

    AutorisationController autorisationController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logowanie);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        loginET = findViewById(R.id.podany_login);
        hasloET = findViewById(R.id.podane_haslo);

        autorisationController = new AutorisationController();
    }

    public void rejestracja(View view) {
        Intent i = new Intent(this, Rejestracja.class);
        startActivity(i);
    }

    public void login(View view) {
        if (TextUtils.isEmpty(loginET.getText())||TextUtils.isEmpty(hasloET.getText())){
            Toast.makeText(getApplicationContext(),"Wprowadź login i hasło",Toast.LENGTH_LONG).show();
        }else {
            if (autorisationController.loginExistence(loginET.getText().toString())) {                    //zwraca true jeśli użytkownik istnieje
                if (autorisationController.verify(loginET.getText().toString(), hasloET.getText().toString())) {           //zwraca true jeśli jest dobre hasło
                    //if (autorisationController.amIaDoctor(loginET.getText().toString())) {           //zwraca true jeśli admin

                    //    SaveSharedPreference.setUserName(this, loginET.getText().toString());
                    //    Intent i = new Intent(this, Glowny_Lekarz.class); //jeśli admin
                    //    startActivity(i);
                    //} else { //czyli zwykły użytkownik

                        SaveSharedPreference.setUserName(this, loginET.getText().toString());
                        Intent i = new Intent(this, MainActivity.class); //jeśli user
                        startActivity(i);
                   // }
                } else {
                    Toast.makeText(getApplicationContext(), "Niepoprawne hasło", Toast.LENGTH_LONG).show();
                }
            } else { //zmieni kolor loginu i hasła na bordowy jeśli są niepoprawne
                Toast.makeText(getApplicationContext(), "Niepoprawny login lub hasło", Toast.LENGTH_LONG).show();
            }
        }
    }
}