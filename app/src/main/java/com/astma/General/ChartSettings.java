package com.astma.General;

import android.widget.TextView;

import com.astma.DTOs.ZazucieDTO;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Locale;

import static com.astma.R.color.complimentaryClr;

public class ChartSettings implements OnChartValueSelectedListener{

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    TextView textView;
    ArrayList<ZazucieDTO> zazucieS;
    ZazucieDTO zazucieSDzis;

    public void draw(BarChart barChart, ArrayList<ZazucieDTO> zazucieS, ArrayList<BarEntry> entries, ZazucieDTO zazucieSDzis, TextView textView){

        this.zazucieS = zazucieS;
        this.zazucieSDzis = zazucieSDzis;
        this.textView = textView;


        if(entries!=null) {
            entries.clear();
            barChart.notifyDataSetChanged();
            barChart.invalidate();
        }

        BarDataSet barDataSet = new BarDataSet(getData(zazucieS, entries), "Dzienne zażycie leku rozszerzającego oskrzela");
        barDataSet.setBarBorderWidth(0.9f);
        barDataSet.setColors(complimentaryClr);
        BarData barData = new BarData(barDataSet);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        IndexAxisValueFormatter formatter = new IndexAxisValueFormatter(dataDescription(zazucieS));
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);

        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setGranularity(1.0f);
        yAxis.setGranularityEnabled(true);
        YAxis yAxisR = barChart.getAxisRight();
        yAxisR.setGranularity(1.0f);
        yAxisR.setGranularityEnabled(true);
        barChart.setDrawGridBackground(false);

        barDataSet.setDrawValues(false);

        barChart.setOnChartValueSelectedListener(this);

        barChart.setDragEnabled(true);
        barChart.getDescription().setText("");
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setData(barData);
        barChart.setFitBars(true);
        barChart.animateXY(500, 1000);
        barChart.invalidate();
    }


    private ArrayList getData(ArrayList<ZazucieDTO> zazucieS, ArrayList<BarEntry> entries){
        entries = new ArrayList<>();
        for (int i = zazucieS.size()-1; i >= 0; i--) { //TE WARTOŚCI IDĄ DO WYKRESU;
            entries.add(new BarEntry((long) zazucieS.size()-1-i, (float) zazucieS.get(i).getDawka()));
        }
        return entries;
    }
    private String[] dataDescription(ArrayList<ZazucieDTO> zazucieS){
        String[]  description;// = new String[]{"Jan", "Feb", "Mar", "Apr", "Mayka", "Jun"};
        ArrayList<String> des = new ArrayList<>();
        if(zazucieS.size()>7){
            for (int i = zazucieS.size()-1; i >= 0; i--) { //TE WARTOŚCI IDĄ DO WYKRESU;
                des.add("");
            }
        } else
            for (int i = zazucieS.size()-1; i >= 0; i--) { //TE WARTOŚCI IDĄ DO WYKRESU;
                des.add(LocalDate.parse(zazucieS.get(i).getDate(), dateTimeFormatter).getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
            }
        description= des.toArray(new String[0]);
        return description;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        int s = (int) e.getY(); //jeśli 0 będzie miało pokazać coś innego
        int j = zazucieS.size()-1-(int) e.getX(); //to jest nr danej w wykresie
        if(s>0) {
            String text1 = ("getY: " + s + " \n getX: " + j);
            String text = ("Data: " + zazucieS.get(j).getDate() + " \n " +
                    "Stosowany lek rozszerzający oskrzela: " + zazucieS.get(j).getLek_os_nazwa() + "\n" +
                    "Stosowany lek przeciwzapalny: " + zazucieS.get(j).getLek_pz_nazwa());
            textView.setText(text);
            zazucieS.get(j).getDate();
        } else{
            String text = ("Dzisiaj użyto inhalator:\n"+ zazucieSDzis.getDawka()+" razy");
            textView.setText(text);
        }
    }

    @Override
    public void onNothingSelected() {
        String text = ("Dzisiaj użyto inhalator:\n"+ zazucieSDzis.getDawka()+" razy");
        textView.setText(text);
    }
}
