package com.astma.General;

import android.widget.Button;

import com.astma.Controllers.ZazuciaController;
import com.astma.DTOs.ZazucieDTO;
import com.astma.R;

import java.util.ArrayList;

import static com.astma.R.drawable.buttoninapp;
import static com.astma.R.drawable.buttoninappselected;

public class Przyciski {


    ArrayList<ZazucieDTO> zazucieS;
    ZazuciaController zazuciaController = new ZazuciaController();

    public ArrayList<ZazucieDTO> przyciski(Button tydzienb, Button miesiacb, Button rokb, int dni, String login){
        tydzienb.setBackgroundResource(buttoninapp);
        miesiacb.setBackgroundResource(buttoninapp);
        rokb.setBackgroundResource(buttoninapp);

        if(dni == 7){
            tydzienb.setBackgroundResource(buttoninappselected);
        }else if(dni ==30){
            miesiacb.setBackgroundResource(buttoninappselected);
        }else {
            rokb.setBackgroundResource(buttoninappselected);
        }
        return zazucieS = zazuciaController.allForPacjent(login, dni);
    }
}
