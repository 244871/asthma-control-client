package com.astma.General;

import android.app.Activity;
import android.drm.DrmStore;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.astma.Controllers.UsersController;
import com.astma.DTOs.LogDTO;
import com.astma.Views.Rejestracja;

import java.util.ArrayList;

public class SpinnerGetter {

    UsersController usersController = new UsersController();
    String[] items;

    public String[] getLekarz() {
        ArrayList<LogDTO> doctorsLogins = usersController.getAllDoctors();
        items = new String[doctorsLogins.size() + 1];
        items[0] = "Wybierz lekarza";
        for (int i = 0; i < doctorsLogins.size(); i++) {
            items[i + 1] = doctorsLogins.get(i).getLogin();
        }
        return items;
    }

    public String[] getPacjents(String login) {
        ArrayList<LogDTO> pacjentsLogins = usersController.getPacjents(login);
        items = new String[pacjentsLogins.size()];
        for (int i = 0; i < pacjentsLogins.size(); i++) {
            items[i] = String.valueOf(pacjentsLogins.get(i).getLogin());
        }
        return items;
    }
}
